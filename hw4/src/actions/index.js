export const addTodo = (text) => ({
  type: 'ADD_TODO',
  text
})

export const deleteTodo = (index) => ({
  type: 'DEL_TODO',
  index
})
