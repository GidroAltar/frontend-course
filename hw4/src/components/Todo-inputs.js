import React, { Component } from 'react';
import './Todo-inputs.css';

class TodoInputs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todoName: ''
        }
    }

    handleTodoNameChange = (e) => {
        this.setState({ todoName: e.target.value });
    }

    handleAddTodoBtnClick = (e) => {
        this.props.addTodo(this.state.todoName);
    }

    render() {
        return (
            <div className="input-group todo-inputs">
                <input type="text" className="form-control" placeholder="Название задачи" value={ this.state.todoName } onChange={ this.handleTodoNameChange } />
                <div className="input-group-btn">
                    <button className="btn btn-default" type="submit" onClick={ this.handleAddTodoBtnClick }>
                        <i className="glyphicon">Добавить</i>
                    </button>
                </div>
            </div>
        );
    }
}

export default TodoInputs;