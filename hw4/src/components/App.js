import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import TodoInputs from './Todo-inputs.js'
import TodoList from './Todo-list.js'
import * as todoActions from '../actions/index.js'


class App extends Component {
  
  render() {
    const { addTodo, deleteTodo } = this.props.todoActions;
    return (
      <div>
        <TodoInputs addTodo={ addTodo } />
        <TodoList todos={ this.props.todos } deleteTodo={ deleteTodo } />
      </div>
    );
  }
}

function mapStateToProps (state) {
  return {
    todos: state
  }
}

function mapDispatchToProps(dispatch) {
  return {
    todoActions: bindActionCreators(todoActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
