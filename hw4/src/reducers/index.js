
const todosReducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return [
        ...state,
        action.text
      ]
    case 'DEL_TODO': 
      return state.slice(0,action.index).concat(state.slice(action.index + 1));
    default:
      return state
  }
}

export default todosReducer
