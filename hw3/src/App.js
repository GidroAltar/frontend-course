import React, { Component } from 'react';
import TodoInputs from './Todo-inputs';
import TodoList from './Todo-list';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: []
    }
  }

  handleAddTodoClick(todoName) {
    if(todoName === '')
      return;
    this.setState(() => {
      this.state.todos.push(todoName)
      return this.state.todos;
    });
  }

  handleDeleteTodoClick(i) {
    this.setState(this.state.todos.splice(i, 1));
  }

  render() {
    return (
      <div>
        <TodoInputs onClick={ todoName => this.handleAddTodoClick(todoName) } />
        <TodoList todos={ this.state.todos } onClick={ i => this.handleDeleteTodoClick(i) } />
      </div>
    );
  }
}

export default App;
