import React, { Component } from 'react';
import './Todo-list.css';

class TodoList extends Component {

    renderTodo(todoName, index) {
        return (
            <div className="list-group-item todo" key={ index }>
                <span className="title">{ todoName }</span>
                <button className="btn btn-default btn-delete-todo" onClick={ () => this.props.onClick(index) }>
                    <span className="glyphicon glyphicon-trash"></span>
                </button>
            </div>
        )
    }

    render() {
        return (
            <div className="list-group">
                {this.props.todos.map((item, index) => (
                    this.renderTodo(item, index)
                ))}
            </div>
        );
    }
}

export default TodoList;