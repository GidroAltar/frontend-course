import React, { Component } from 'react';
import './Todo-inputs.css';

class TodoInputs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todoName: ''
        }
    }

    handleTodoNameChange(e) {
        this.setState({ todoName: e.target.value });
    }

    render() {
        return (
            <div className="input-group todo-inputs">
                <input type="text" className="form-control" placeholder="Название задачи" value={ this.state.todoName } onChange={ this.handleTodoNameChange.bind(this) } />
                <div className="input-group-btn">
                    <button className="btn btn-default" type="submit" onClick={ () => this.props.onClick(this.state.todoName) }>
                        <i className="glyphicon">Добавить</i>
                    </button>
                </div>
            </div>
        );
    }
}

export default TodoInputs;