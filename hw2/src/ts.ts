﻿abstract class Vehicle {
    name: string;
    description: string;

    constructor(name: string, description: string) {
        this.description = description;
        this.name = name;
    }

    showLabel(): string {
        return this.name;
    }

    showInfo(): string {
        return `Название: ${this.name} </br>
                        Примечание: ${this.description}`;
    }
}

class Truck extends Vehicle {
    carrying: number;
    constructor(name: string, description: string, carrying: number) {
        super(name, description);
        this.carrying = carrying;
    }

    showLabel(): string {
        return `${super.showLabel()} ${this.carrying} т`;
    }

    showInfo(): string {
        return `Тип: грузовая </br>
                    Грузоподъемность: ${this.carrying} </br>
                    ${super.showInfo()}`;
    }
}

class Car extends Vehicle {
    velocity: number;
    constructor(name: string, description: string, velocity: number) {
        super(name, description);
        this.velocity = velocity;
    }

    showLabel(): string {
        return `${super.showLabel()} ${this.velocity} км/ч`;
    }

    showInfo(): string {
        return `Тип: легковая </br>
                    Скорость: ${this.velocity} </br>
                    ${super.showInfo()}`;
    }
}

//находим элементы ввода
let vehicleNameElem = <HTMLInputElement>document.getElementById("vehicle-name");
let vehicleWeightVelocityElem = <HTMLInputElement>document.getElementById("vehicle-weight");
let vehicleDescriptionElem = <HTMLInputElement>document.getElementById("vehicle-description");
let isCarElem = <HTMLInputElement>document.getElementById("is-car");
let isTruckElem = <HTMLInputElement>document.getElementById("is-truck");
let vehicleListElem = document.getElementById("vehicle-list");
let vehicleInfoElem = document.getElementById("vehicle-info");
let vehicleWeightLabelElem = document.getElementById("vehicle-weight-label");
let addVehicleElem = <HTMLInputElement>document.getElementById("add-vehicle");

let vehicles: Array<Vehicle> = new Array<Vehicle>();
//создает машину по введенной инфомрации и добавляет в список
addVehicleElem.onclick = function(e) {
    let newVehicle: Vehicle;
    let name = vehicleNameElem.value;
    let weightVelocity = Number(vehicleWeightVelocityElem.value);
    let vehicleType = isCarElem.checked ? "car" : "truck";
    let description = vehicleDescriptionElem.value;

    if (vehicleType === "car")
        newVehicle = new Car(name, description, weightVelocity);
    else if (vehicleType === "truck")
        newVehicle = new Truck(name, description, weightVelocity);

    vehicles.push(newVehicle);

    //выводим машину в список существующих 
    let newVehicleElem = document.createElement("li");
    newVehicleElem.onclick = function () {
        vehicleInfoElem.innerHTML = newVehicle.showInfo.bind(newVehicle)();
    }
    newVehicleElem.innerHTML = newVehicle.showLabel();
    vehicleListElem.appendChild(newVehicleElem);
}
//меняем название параметра в зависимости от выбранного типа машины
isCarElem.onchange = function(e) {
    vehicleWeightLabelElem.innerHTML = "скорость";
}

isTruckElem.onchange = function(e) {
    vehicleWeightLabelElem.innerHTML = "грузоподъемность";
}