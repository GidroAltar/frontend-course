;(function() {
        //находим элементы ввода
        var vehicleNameElem = document.getElementById("vehicle-name");
        var vehicleWeightVelocityElem = document.getElementById("vehicle-weight");
        var vehicleDescriptionElem = document.getElementById("vehicle-description");
        var isCarElem = document.getElementById("is-car");
        var isTruckElem = document.getElementById("is-truck");
        var vehicleListElem = document.getElementById("vehicle-list");
        var vehicleInfoElem = document.getElementById("vehicle-info");
        var vehicleWeightLabelElem = document.getElementById("vehicle-weight-label");
        var addVehicleElem = document.getElementById("add-vehicle");
        
        var vehicles = [];
        //создает машину по введенной инфомрации и добавляет в список
        addVehicleElem.onclick = function (e) {
            var newVehicle;
            var name = vehicleNameElem.value;
            var weightVelocity = vehicleWeightVelocityElem.value;
            var vehicleType = isCarElem.checked ? "car" : "truck";
            var description = vehicleDescriptionElem.value;

            if (vehicleType === "car")
                newVehicle = new Car(name, description, weightVelocity);
            else if (vehicleType === "truck")
                newVehicle = new Truck(name, description, weightVelocity);
            
            var vehicleObject = {
                vehicle: newVehicle,
                onClick: newVehicle.showInfo.bind(newVehicle),
            }
            vehicles.push(vehicleObject);

            //выводим машину в список существующих 
            var newVehicleElem = document.createElement("li");
            newVehicleElem.onclick = function() {
                vehicleInfoElem.innerHTML = vehicleObject.onClick();
            }
            newVehicleElem.innerHTML = newVehicle.showLabel();
            vehicleListElem.appendChild(newVehicleElem);
        }
        //меняем название параметра в зависимости от выбранного типа машины
        isCarElem.onchange = function (e) {
            vehicleWeightLabelElem.innerHTML = "скорость";
        }
        isTruckElem.onchange = function (e) {
            vehicleWeightLabelElem.innerHTML = "грузоподъемность";
        }

        //класс Машина
        function Vehicle(vname, description) {
            // сохранение параметров name, description
            this.vname = vname;
            this.description = description;
        }
        //класс Грузовая машина
        function Truck(vname, description, carrying) {
            Vehicle.apply(this, arguments);
            this.carrying = carrying;
            // информация для отображения в списке машин
            this.showLabel = function () {
                return this.vname + " " + this.carrying + " т";
            }

            // информация для отображения под списком машин,
            // при выборе строки в списке
            this.showInfo = function () {
                return "Тип: грузовая" + "<br>" + "Название: " + this.vname
                    + "<br>" + "Грузоподъемность: " + this.carrying + "<br>"
                    + "Примечание: " + this.description;
            }

        }
        Truck.prototype = Object.create(Vehicle);
        //класс Легковая машина
        function Car(vname, description, velocity) {
            Vehicle.apply(this, arguments);
            this.velocity = velocity;
            // информация для отображения в списке машин
            this.showLabel = function () {
                return this.vname + " " + this.velocity + " км/ч";
            }

            // информация для отображения под списком машин,
            // при выборе строки в списке
            this.showInfo = function () {
                return "Тип: легковая" + "<br>" + "Название: " + this.vname
                    + "<br>" + "Скорость: " + this.velocity + "<br>"
                    + "Примечание: " + this.description;
            }
        }
        Car.prototype = Object.create(Vehicle);
})();